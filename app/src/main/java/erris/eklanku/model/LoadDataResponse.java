package erris.eklanku.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoadDataResponse {
    @SerializedName("status")
    private String status;

    @SerializedName("error")
    private String error;

    @SerializedName("result")
    private List<DataNominal> result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<DataNominal> getResult() {
        return result;
    }

    public void setResult(List<DataNominal> result) {
        this.result = result;
    }
}
