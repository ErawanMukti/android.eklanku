package erris.eklanku.model;

import com.google.gson.annotations.SerializedName;

public class DataLaporanTransaksi {
    @SerializedName("transaksi_id")
    private String transaksi_id;

    @SerializedName("transaksi_code")
    private String transaksi_code;

    @SerializedName("transaksi_amount")
    private Double transaksi_amount;

    @SerializedName("transaksi_status")
    private String transaksi_status;

    @SerializedName("transaksi_date")
    private String transaksi_date;

    @SerializedName("transaksi_desc")
    private String transaksi_desc;

    @SerializedName("ip_addrs")
    private String ip_addrs;

    public String getTransaksiId() {
        return transaksi_id;
    }

    public void setTransaksiId(String transaksi_id) {
        this.transaksi_id = transaksi_id;
    }

    public String getTransaksiCode() {
        return transaksi_code;
    }

    public void setTransaksiCode(String transaksi_code) {
        this.transaksi_code = transaksi_code;
    }

    public Double getTransaksiAmount() {
        return transaksi_amount;
    }

    public void setTransaksiAmount(Double transaksi_amount) {
        this.transaksi_amount = transaksi_amount;
    }

    public String getTransaksiStatus() {
        return transaksi_status;
    }

    public void setTransaksiStatus(String transaksi_status) {
        this.transaksi_status = transaksi_status;
    }

    public String getTransaksiDate() {
        return transaksi_date;
    }

    public void setTransaksiDate(String transaksi_date) {
        this.transaksi_date = transaksi_date;
    }

    public String getTransaksiDesc() {
        return transaksi_desc;
    }

    public void setTransaksiDesc(String transaksi_desc) {
        this.transaksi_desc = transaksi_desc;
    }

    public String getIpAddrs() {
        return ip_addrs;
    }

    public void setIpAddrs(String ip_addrs) {
        this.ip_addrs = ip_addrs;
    }
}
