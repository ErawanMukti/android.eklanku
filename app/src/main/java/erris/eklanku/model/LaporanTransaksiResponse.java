package erris.eklanku.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LaporanTransaksiResponse {
    @SerializedName("status")
    private String status;

    @SerializedName("error")
    private String error;

    @SerializedName("data")
    private List<DataLaporanTransaksi> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<DataLaporanTransaksi> getData() {
        return data;
    }

    public void setData(List<DataLaporanTransaksi> data) {
        this.data = data;
    }

}
