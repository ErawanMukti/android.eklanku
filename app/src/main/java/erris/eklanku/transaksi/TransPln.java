package erris.eklanku.transaksi;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import erris.eklanku.R;
import erris.eklanku.model.DataNominal;
import erris.eklanku.model.DataTransBeli;
import erris.eklanku.model.LoadDataResponse;
import erris.eklanku.model.TransBeliResponse;
import erris.eklanku.rest.ApiClient;
import erris.eklanku.rest.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransPln extends AppCompatActivity {

    private static String[] tipe_trans = {"Token Listrik", "Tagihan Listrik"};
    private static String load_type    = "token_nominal";

    SharedPreferences prefs;
    Spinner spnTipe, spnNominal;
    LinearLayout laySpnNominal;
    EditText txtNo;
    TextInputLayout layoutNo;
    Button btnBayar;
    String id_member, load_id = "PLN", selected_nominal;
    ApiInterface mApiInterface;
    Dialog loadingDialog;
    String[] nominal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trans_pln);

        prefs         = getSharedPreferences("app", Context.MODE_PRIVATE);
        spnTipe       = (Spinner) findViewById(R.id.spnTransPln);
        spnNominal    = (Spinner) findViewById(R.id.spnTransPlnNominal);
        txtNo         = (EditText) findViewById(R.id.txtTransPlnNo);
        layoutNo      = (TextInputLayout) findViewById(R.id.txtLayoutTransPlnNo);
        laySpnNominal = (LinearLayout) findViewById(R.id.spnLayoutTransPlnNominal);
        btnBayar      = (Button) findViewById(R.id.btnTransPlnBayar);
        id_member     = prefs.getString("auth_id", "");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Transaksi PLN");

        txtNo.addTextChangedListener(new txtWatcher(txtNo));

        mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, tipe_trans);
        spnTipe.setAdapter(adapter);
        spnTipe.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ( position == 0 ) {
                    laySpnNominal.setVisibility(View.VISIBLE);
                    btnBayar.setText("Beli Token Listrik");
                    load_id = "PLN";
                } else {
                    laySpnNominal.setVisibility(View.GONE);
                    btnBayar.setText("Cek Tagihan Listrik");
                    load_id = "PLNPOST";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !validateIdpel() ) {
                    return;
                }
                cek_transaksi();
            }
        });

        load_data();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class txtWatcher implements TextWatcher {

        private View view;

        private txtWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            validateIdpel();
        }
    }

    private boolean validateIdpel() {
        String id_pel = txtNo.getText().toString().trim();

        if ( id_pel.isEmpty() ) {
            layoutNo.setError("Kolom id pelanggan tidak boleh kosong");
            layoutNo.setBackgroundColor(getResources().getColor(android.R.color.background_light));
            requestFocus(txtNo);
            return false;
        }

        if ( id_pel.length() < 8 ) {
            layoutNo.setError("Masukkan minimal 8 digit id pelanggan");
            layoutNo.setBackgroundColor(getResources().getColor(android.R.color.background_light));
            requestFocus(txtNo);
            return false;
        }

        layoutNo.setErrorEnabled(false);
        return true;
    }

    private void requestFocus(View view) {
        if ( view.requestFocus() ) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void load_data() {
        loadingDialog = ProgressDialog.show(TransPln.this, "Harap Tunggu", "Mengambil Data...");
        loadingDialog.setCanceledOnTouchOutside(true);
        Call<LoadDataResponse> dataCall = mApiInterface.postLoadData(load_type, load_id);
        dataCall.enqueue(new Callback<LoadDataResponse>() {
            @Override
            public void onResponse(Call<LoadDataResponse> call, Response<LoadDataResponse> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    String status   = response.body().getStatus();
                    String error    = response.body().getError();

                    if ( status.equals("OK") ) {
                        final List<DataNominal> result = response.body().getResult();
                        nominal                        = new String[result.size()];
                        selected_nominal               = result.get(0).getH2hCode();

                        for ( int i=0; i<result.size(); i++ ) {
                            nominal[i] = result.get(i).getProductName();
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, nominal);
                        spnNominal.setAdapter(adapter);
                        spnNominal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                selected_nominal = result.get(position).getH2hCode();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    } else {
                        Toast.makeText(getBaseContext(), "Terjadi kesalahan:\n" + error, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getBaseContext(), getResources().getString(R.string.error_api), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoadDataResponse> call, Throwable t) {
                loadingDialog.dismiss();
                Toast.makeText(getBaseContext(), getResources().getString(R.string.error_api), Toast.LENGTH_SHORT).show();
                Log.d("API_LOADDATA", t.getMessage().toString());
            }
        });
    }

    private void cek_transaksi() {
        loadingDialog = ProgressDialog.show(TransPln.this, "Harap Tunggu", "Cek Transaksi...");
        loadingDialog.setCanceledOnTouchOutside(true);

        Call<TransBeliResponse> transBeliCall = mApiInterface.postTransBeli(id_member, load_id, selected_nominal, txtNo.getText().toString(), "token");
        transBeliCall.enqueue(new Callback<TransBeliResponse>() {
            @Override
            public void onResponse(Call<TransBeliResponse> call, Response<TransBeliResponse> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    String status = response.body().getStatus();
                    String error  = response.body().getError();

                    if ( status.equals("OK") ) {
                        List<DataTransBeli> trans = response.body().getResult();
                        Intent inKonfirmasi       = new Intent(getBaseContext(), TransKonfirmasi.class);
                        inKonfirmasi.putExtra("transaksi", trans.get(0).getTransaksi());
                        inKonfirmasi.putExtra("harga", trans.get(0).getHarga());
                        inKonfirmasi.putExtra("id_pel", trans.get(0).getIdPel());
                        inKonfirmasi.putExtra("jenis", trans.get(0).getJenis());
                        inKonfirmasi.putExtra("pin", trans.get(0).getPin());
                        inKonfirmasi.putExtra("cmd_save", trans.get(0).getCmdSave());
                        startActivity(inKonfirmasi);
                    } else {
                        Toast.makeText(getBaseContext(), error, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getBaseContext(), getResources().getString(R.string.error_api), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TransBeliResponse> call, Throwable t) {
                loadingDialog.dismiss();
                Toast.makeText(getBaseContext(), getResources().getString(R.string.error_api), Toast.LENGTH_SHORT).show();
                Log.d("API_TRANSBELI", t.getMessage().toString());
            }
        });
    }
}
