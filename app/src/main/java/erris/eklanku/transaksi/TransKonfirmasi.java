package erris.eklanku.transaksi;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import erris.eklanku.R;
import erris.eklanku.model.DataTransBeli;
import erris.eklanku.model.LoadDataResponse;
import erris.eklanku.model.TransBeliResponse;
import erris.eklanku.rest.ApiClient;
import erris.eklanku.rest.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransKonfirmasi extends AppCompatActivity {

    Bundle extras;
    SharedPreferences prefs;
    TextView lblContent;
    Button btnSave;
    ApiInterface mApiInterface;
    Dialog loadingDialog;
    String id_member, transaksi, harga, jenis, id_pel, pin, cmd_save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trans_konfirmasi);

        getSupportActionBar().setTitle("Konfirmasi Transasi");

        extras     = getIntent().getExtras();
        prefs      = getSharedPreferences("app", Context.MODE_PRIVATE);
        lblContent = (TextView) findViewById(R.id.lblKonfirmasiContent);
        btnSave    = (Button) findViewById(R.id.btnKonfirmasiSave);

        id_member  = prefs.getString("auth_id", "");
        jenis      = extras.getString("jenis");
        id_pel     = extras.getString("id_pel");
        pin        = extras.getString("pin");
        cmd_save   = extras.getString("cmd_save");
        transaksi  = extras.getString("transaksi");
        harga      = NumberFormat.getNumberInstance(Locale.US).format(extras.getDouble("harga"));

        mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        lblContent.setText("Anda yakin akan melanjutkan transaksi " + transaksi +
                " senilai Rp. " + harga +
                " ke nomer tujuan " + id_pel + "?\n" +
                "Silahkan klik tombol Proses untuk melanjutkan");

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                konfirm_transaksi();
            }
        });
    }

    private void konfirm_transaksi() {
        loadingDialog = ProgressDialog.show(TransKonfirmasi.this, "Harap Tunggu", "Konfirmasi Pembayaran...");
        loadingDialog.setCanceledOnTouchOutside(true);

        Call<TransBeliResponse> transKonfirmCall = mApiInterface.postTransConfirm(id_member, jenis, id_pel, pin, cmd_save);
        transKonfirmCall.enqueue(new Callback<TransBeliResponse>() {
            @Override
            public void onResponse(Call<TransBeliResponse> call, Response<TransBeliResponse> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    String status = response.body().getStatus();
                    String error  = response.body().getError();

                    if ( status.equals("OK") ) {
                        Intent inThankYou = new Intent(getBaseContext(), TransThankyou.class);
                        startActivity(inThankYou);
                        finish();
                    } else {
                        Toast.makeText(getBaseContext(), error, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getBaseContext(), getResources().getString(R.string.error_api), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TransBeliResponse> call, Throwable t) {
                loadingDialog.dismiss();
                Toast.makeText(getBaseContext(), getResources().getString(R.string.error_api), Toast.LENGTH_SHORT).show();
                Log.d("API_TRANSCONFIRM", t.getMessage().toString());
            }
        });
    }
}
