package erris.eklanku.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import erris.eklanku.R;
import erris.eklanku.model.DataLaporanTransaksi;

public class RecLapTransAdapter extends RecyclerView.Adapter<RecLapTransAdapter.ViewHolder> {
    private Context context;
    private List<DataLaporanTransaksi> dataLaporan;

    public RecLapTransAdapter(Context c, List<DataLaporanTransaksi> data) {
        this.context = c;
        this.dataLaporan = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_laptransaksi, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.txtRowDate.setText(dataLaporan.get(position).getTransaksiDate().substring(0, 10));
        holder.txtRowStatus.setText(dataLaporan.get(position).getTransaksiStatus());
        holder.txtRowDesc.setText(dataLaporan.get(position).getTransaksiDesc() +
                "Ke nomor: " + dataLaporan.get(position).getIpAddrs());
    }

    @Override
    public int getItemCount() {
        return dataLaporan.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView txtRowDate;
        private TextView txtRowStatus;
        private TextView txtRowDesc;

        public ViewHolder(View itemView) {
            super(itemView);

            txtRowDate   = (TextView) itemView.findViewById(R.id.lbl_rlaptrans_date);
            txtRowStatus = (TextView) itemView.findViewById(R.id.lbl_rlaptrans_status);
            txtRowDesc   = (TextView) itemView.findViewById(R.id.lbl_rlaptrans_desc);
            context      = itemView.getContext();
        }

        @Override
        public void onClick(View v) {

        }
    }
}
