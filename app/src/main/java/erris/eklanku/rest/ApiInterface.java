package erris.eklanku.rest;

import erris.eklanku.model.LaporanTransaksiResponse;
import erris.eklanku.model.LoadDataResponse;
import erris.eklanku.model.LoginResponse;
import erris.eklanku.model.RegisterResponse;
import erris.eklanku.model.TransBeliResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {
    @FormUrlEncoded
    @POST("users/login")
    Call<LoginResponse> postLogin(
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("users/register")
    Call<RegisterResponse> postRegister (
            @Field("name") String name,
            @Field("phone") String phone,
            @Field("email") String email,
            @Field("password") String password,
            @Field("re_password") String re_password,
            @Field("referal") String referal
    );

    @GET("users/account")
    Call<LoginResponse> getAccount ( @Query("id") String id );

    @FormUrlEncoded
    @POST("pembayaran/loaddata")
    Call<LoadDataResponse> postLoadData (
            @Field("load_type") String load_type,
            @Field("load_id") String load_id
    );

    @FormUrlEncoded
    @POST("pembayaran/transbeli")
    Call<TransBeliResponse> postTransBeli (
            @Field("id") String id,
            @Field("jenis") String jenis,
            @Field("nominal") String nominal,
            @Field("id_pel") String id_pel,
            @Field("nm") String nm
    );

    @FormUrlEncoded
    @POST("pembayaran/transconfirm")
    Call<TransBeliResponse> postTransConfirm (
            @Field("id") String id,
            @Field("jenis") String jenis,
            @Field("id_pel") String id_pel,
            @Field("pin") String pin,
            @Field("cmd_save") String cmd_save
    );

    @FormUrlEncoded
    @POST("laporan/transaksi")
    Call<LaporanTransaksiResponse> postLapTransaksi ( @Field("id") String id );

}
