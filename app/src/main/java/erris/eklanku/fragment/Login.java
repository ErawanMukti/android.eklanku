package erris.eklanku.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import erris.eklanku.HalamanUtama;
import erris.eklanku.R;
import erris.eklanku.model.DataMember;
import erris.eklanku.model.LoginResponse;
import erris.eklanku.rest.ApiClient;
import erris.eklanku.rest.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends Fragment implements View.OnClickListener {

    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    TextInputLayout layoutEmail, layoutPass;
    EditText txtEmail, txtPass;
    Button btnLogin;
    TextView lblDaftar;
    ApiInterface mApiInterface;
    Context context;
    Dialog loadingDialog;

    public Login() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);

        context     = getActivity();
        prefs       = getActivity().getSharedPreferences("app", Context.MODE_PRIVATE);
        layoutEmail = (TextInputLayout) rootView.findViewById(R.id.txtLayoutLoginEmail);
        layoutPass  = (TextInputLayout) rootView.findViewById(R.id.txtLayoutLoginPassword);
        txtEmail    = (EditText) rootView.findViewById(R.id.txtLoginEmail);
        txtPass     = (EditText) rootView.findViewById(R.id.txtLoginPassword);
        btnLogin    = (Button) rootView.findViewById(R.id.btnLogin);
        lblDaftar   = (TextView) rootView.findViewById(R.id.lblLoginRegister);

        txtEmail.addTextChangedListener(new txtWatcher(txtEmail));
        txtPass.addTextChangedListener(new txtWatcher(txtPass));

        btnLogin.setOnClickListener(this);
        lblDaftar.setOnClickListener(this);

        mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        return rootView;
    }

    private class txtWatcher implements TextWatcher {

        private View view;

        private txtWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch ( view.getId() ) {
                case R.id.txtLoginEmail:
                    validateEmail();
                    break;

                case R.id.txtLoginPassword:
                    validatePassword();
                    break;
            }
        }

    }

    private boolean validateEmail() {
        String email = txtEmail.getText().toString().trim();

        if ( email.isEmpty() ) {
            layoutEmail.setError("Kolom email tidak boleh kosong");
            requestFocus(txtEmail);
            return false;
        }

        if ( !isValidEmail(email) ) {
            layoutEmail.setError("Format email tidak valid");
            requestFocus(txtEmail);
            return false;
        }

        layoutEmail.setErrorEnabled(false);
        return true;
    }

    private boolean validatePassword() {
        String pass = txtPass.getText().toString().trim();

        if ( pass.isEmpty() ) {
            layoutPass.setError("Kolom password tidak boleh kosong");
            requestFocus(txtPass);
            return false;
        }

        layoutPass.setErrorEnabled(false);
        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if ( view.requestFocus() ) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnLogin:
                if ( !validateEmail() || !validatePassword() ) {
                    return;
                }

                loadingDialog = ProgressDialog.show(context, "Harap Tunggu", "Mengambil Data...");
                loadingDialog.setCanceledOnTouchOutside(true);

                String email = txtEmail.getText().toString().trim();
                String pass  = txtPass.getText().toString().trim();

                Call<LoginResponse> userCall = mApiInterface.postLogin(email, pass);
                userCall.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        loadingDialog.dismiss();
                        if (response.isSuccessful()) {
                            String status = response.body().getStatus();
                            String error  = response.body().getError();

                            if ( status.equals("OK") ) {
                                List<DataMember> member = response.body().getMember();

                                editor = prefs.edit();
                                editor.putString("auth_id", member.get(0).getId());
                                editor.putString("auth_eklankuid", member.get(0).getEklankuId());
                                editor.putString("auth_name", member.get(0).getName());
                                editor.putString("auth_email", member.get(0).getEmail());
                                editor.commit();

                                Intent in_beranda = new Intent(context, HalamanUtama.class);
                                startActivity(in_beranda);
                                getActivity().finish();
                            } else {
                                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context, getResources().getString(R.string.error_api), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        loadingDialog.dismiss();
                        Toast.makeText(context, getResources().getString(R.string.error_api), Toast.LENGTH_SHORT).show();
                        Log.d("API_LOGIN", t.getMessage().toString());
                    }
                });

                break;

            case R.id.lblLoginRegister:
                Fragment fr_register = new Daftar();
                FragmentManager fm_register = getActivity().getSupportFragmentManager();
                fm_register.beginTransaction()
                        .replace(R.id.content_frame, fr_register)
                        .commit();
                break;
        }
    }

}
