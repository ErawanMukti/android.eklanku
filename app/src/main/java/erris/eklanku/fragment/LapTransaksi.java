package erris.eklanku.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import erris.eklanku.R;
import erris.eklanku.adapter.RecLapTransAdapter;
import erris.eklanku.model.DataLaporanTransaksi;
import erris.eklanku.model.LaporanTransaksiResponse;
import erris.eklanku.rest.ApiClient;
import erris.eklanku.rest.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LapTransaksi extends Fragment {

    Context context;
    SharedPreferences prefs;
    RecyclerView rvLap;
    ApiInterface mApiInterface;
    Dialog loadingDialog;
    String eklan_id;
    RecLapTransAdapter adapter;

    public LapTransaksi() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_lap_transaksi, container, false);

        context  = getActivity();
        prefs    = getActivity().getSharedPreferences("app", Context.MODE_PRIVATE);
        eklan_id = prefs.getString("auth_eklankuid", "");

        rvLap = (RecyclerView) rootView.findViewById(R.id.rvLapTransaksi);
        rvLap.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        rvLap.setLayoutManager(layoutManager);

        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        load_data();

        return rootView;
    }

    private void load_data() {
        loadingDialog = ProgressDialog.show(context, "Harap Tunggu", "Mengambil Data...");
        loadingDialog.setCanceledOnTouchOutside(true);

        Call<LaporanTransaksiResponse> dataCall = mApiInterface.postLapTransaksi(eklan_id);
        dataCall.enqueue(new Callback<LaporanTransaksiResponse>() {
            @Override
            public void onResponse(Call<LaporanTransaksiResponse> call, Response<LaporanTransaksiResponse> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    String status   = response.body().getStatus();
                    String error    = response.body().getError();

                    if ( status.equals("OK") ) {
                        List<DataLaporanTransaksi> data = response.body().getData();
                        adapter = new RecLapTransAdapter(context, data);
                        rvLap.setAdapter(adapter);
                    } else {
                        Toast.makeText(context, "Terjadi kesalahan:\n" + error, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, getResources().getString(R.string.error_api), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LaporanTransaksiResponse> call, Throwable t) {
                loadingDialog.dismiss();
                Toast.makeText(context, getResources().getString(R.string.error_api), Toast.LENGTH_SHORT).show();
                Log.d("API_LAPTRANS", t.getMessage().toString());
            }
        });
    }

}
