package erris.eklanku.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import erris.eklanku.R;
import erris.eklanku.model.RegisterResponse;
import erris.eklanku.rest.ApiClient;
import erris.eklanku.rest.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Daftar extends Fragment implements View.OnClickListener {

    TextInputLayout layoutNama, layoutPhone, layoutEmail, layoutPass, layoutPass2;
    EditText txtNama, txtPhone, txtEmail, txtPass, txtPass2, txtReferal;
    Button btnDaftar;
    TextView lblLogin;
    ApiInterface mApiInterface;
    Context context;
    Dialog loadingDialog;

    public Daftar() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_daftar, container, false);

        context     = getActivity();
        layoutNama  = (TextInputLayout) rootView.findViewById(R.id.txtLayoutRegisterNama);
        layoutPhone = (TextInputLayout) rootView.findViewById(R.id.txtLayoutRegisterPhone);
        layoutEmail = (TextInputLayout) rootView.findViewById(R.id.txtLayoutRegisterEmail);
        layoutPass  = (TextInputLayout) rootView.findViewById(R.id.txtLayoutRegisterPassword);
        layoutPass2 = (TextInputLayout) rootView.findViewById(R.id.txtLayoutRegisterPassword2);
        txtNama     = (EditText) rootView.findViewById(R.id.txtRegisterNama);
        txtPhone    = (EditText) rootView.findViewById(R.id.txtRegisterPhone);
        txtEmail    = (EditText) rootView.findViewById(R.id.txtRegisterEmail);
        txtPass     = (EditText) rootView.findViewById(R.id.txtRegisterPassword);
        txtPass2    = (EditText) rootView.findViewById(R.id.txtRegisterPassword2);
        txtReferal  = (EditText) rootView.findViewById(R.id.txtRegisterReferal);
        btnDaftar   = (Button) rootView.findViewById(R.id.btnRegister);
        lblLogin    = (TextView) rootView.findViewById(R.id.lblRegisterLogin);

        txtNama.addTextChangedListener(new txtWatcher(txtNama));
        txtPhone.addTextChangedListener(new txtWatcher(txtPhone));
        txtEmail.addTextChangedListener(new txtWatcher(txtEmail));
        txtPass.addTextChangedListener(new txtWatcher(txtPass));
        txtPass2.addTextChangedListener(new txtWatcher(txtPass2));

        btnDaftar.setOnClickListener(this);
        lblLogin.setOnClickListener(this);

        mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        return rootView;
    }

    private class txtWatcher implements TextWatcher {

        private View view;

        private txtWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch ( view.getId() ) {
                case R.id.txtRegisterNama:
                    validateName();
                    break;

                case R.id.txtRegisterPhone:
                    validatePhone();
                    break;

                case R.id.txtRegisterEmail:
                    validateEmail();
                    break;

                case R.id.txtRegisterPassword:
                    validatePassword();
                    break;

                case R.id.txtRegisterPassword2:
                    validatePassword2();
                    break;
            }
        }
    }

    private boolean validateName() {
        String nama = txtNama.getText().toString().trim();

        if ( nama.isEmpty() ) {
            layoutNama.setError("Kolom nama tidak boleh kosong");
            requestFocus(txtNama);
            return false;
        }

        layoutNama.setErrorEnabled(false);
        return true;
    }

    private boolean validatePhone() {
        String phone = txtPhone.getText().toString().trim();

        if ( phone.isEmpty() ) {
            layoutPhone.setError("Kolom no telepon tidak boleh kosong");
            requestFocus(txtPhone);
            return false;
        }

        layoutPhone.setErrorEnabled(false);
        return true;
    }

    private boolean validateEmail() {
        String email = txtEmail.getText().toString().trim();

        if ( email.isEmpty() ) {
            layoutEmail.setError("Kolom email tidak boleh kosong");
            requestFocus(txtEmail);
            return false;
        }

        if ( !isValidEmail(email) ) {
            layoutEmail.setError("Format email tidak valid");
            requestFocus(txtEmail);
            return false;
        }

        layoutEmail.setErrorEnabled(false);
        return true;
    }

    private boolean validatePassword() {
        String pass = txtPass.getText().toString().trim();

        if ( pass.isEmpty() ) {
            layoutPass.setError("Kolom password tidak boleh kosong");
            requestFocus(txtPass);
            return false;
        }

        layoutPass.setErrorEnabled(false);
        return true;
    }

    private boolean validatePassword2() {
        String pass  = txtPass.getText().toString().trim();
        String pass2 = txtPass2.getText().toString().trim();

        if ( pass2.isEmpty() ) {
            layoutPass2.setError("Kolom konfirmasi password tidak boleh kosong");
            requestFocus(txtPass2);
            return false;
        }

        if ( !pass.equals(pass2) ) {
            layoutPass2.setError("Konfirmasi password tidak sama");
            requestFocus(txtPass2);
            return false;
        }

        layoutPass2.setErrorEnabled(false);
        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if ( view.requestFocus() ) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnRegister:
                if ( !validateName() || !validatePhone() || !validateEmail() || !validatePassword() || !validatePassword2() ) {
                    return;
                }

                loadingDialog = ProgressDialog.show(context, "Harap Tunggu", "Registrasi User...");
                loadingDialog.setCanceledOnTouchOutside(true);

                String nama  = txtNama.getText().toString().trim();
                String phone = txtPhone.getText().toString().trim();
                String email = txtEmail.getText().toString().trim();
                String pass  = txtPass.getText().toString().trim();
                String pass2 = txtPass2.getText().toString().trim();
                String ref   = txtReferal.getText().toString().trim();

                Call<RegisterResponse> userCall = mApiInterface.postRegister(nama, phone, email, pass, pass2, ref);
                userCall.enqueue(new Callback<RegisterResponse>() {
                    @Override
                    public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                        loadingDialog.dismiss();
                        if (response.isSuccessful()) {
                            String status = response.body().getStatus();
                            String error  = response.body().getError();
                            String msg    = response.body().getMessage();

                            if ( status.equals("OK") ) {
                                Toast.makeText(context, "Pendaftaran berhasil", Toast.LENGTH_SHORT).show();

                                Fragment fragment = new Login();
                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                fragmentManager.beginTransaction()
                                        .replace(R.id.content_frame, fragment)
                                        .commit();

                            } else {
                                Toast.makeText(context, "Terjadi kesalahan:\n" + msg, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context, getResources().getString(R.string.error_api), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<RegisterResponse> call, Throwable t) {
                        loadingDialog.dismiss();
                        Toast.makeText(context, getResources().getString(R.string.error_api), Toast.LENGTH_SHORT).show();
                        Log.d("API_REGISTER", t.getMessage().toString());
                    }
                });

                break;

            case R.id.lblRegisterLogin:
                Fragment fragment = new Login();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame, fragment)
                        .commit();
                break;
        }
    }
}
