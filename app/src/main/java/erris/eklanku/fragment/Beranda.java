package erris.eklanku.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;

import com.takusemba.multisnaprecyclerview.MultiSnapRecyclerView;

import erris.eklanku.R;
import erris.eklanku.transaksi.TransBpjs;
import erris.eklanku.transaksi.TransPaket;
import erris.eklanku.transaksi.TransPdam;
import erris.eklanku.transaksi.TransPln;
import erris.eklanku.adapter.HorizontalAdapter;
import erris.eklanku.transaksi.TransPulsa;
import erris.eklanku.transaksi.TransTagihan;
import erris.eklanku.transaksi.TransTelkom;
import erris.eklanku.transaksi.TransTv;
import erris.eklanku.transaksi.TransVoucher;

public class Beranda extends Fragment implements View.OnClickListener {

    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;

    Context context;
    SharedPreferences prefs;
    ViewFlipper viewFlipper;
    Button  btnBayarPln, btnBayarPulsa, btnBayarVoucher, btnBayarPdam,
            btnBayarTagihan, btnBayarBpjs, btnBayarTv, btnBayarTelkom, btnBayarPaket;
    RelativeLayout relativeLayout;

    String[] data_rt = {
            "Coming Soon",
            "Coming Soon",
            "Coming Soon",
            "Coming Soon",
            "Coming Soon"
    };
    String[] data_kecantikan = {
            "Coming Soon",
            "Coming Soon",
            "Coming Soon",
            "Coming Soon",
            "Coming Soon"
    };
    String[] data_makanan = {
            "Coming Soon",
            "Coming Soon",
            "Coming Soon",
            "Coming Soon",
            "Coming Soon"
    };
    String[] data_busanapria = {
            "Coming Soon",
            "Coming Soon",
            "Coming Soon",
            "Coming Soon",
            "Coming Soon"
    };
    String[] data_busanawanita = {
            "Coming Soon",
            "Coming Soon",
            "Coming Soon",
            "Coming Soon",
            "Coming Soon"
    };


    public Beranda() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_beranda, container, false);

        prefs   = getActivity().getSharedPreferences("app", Context.MODE_PRIVATE);
        context = getActivity();

        viewFlipper = (ViewFlipper) rootView.findViewById(R.id.flipperBerandaPromo);
        init_flipper();

        HorizontalAdapter rtAdapter = new HorizontalAdapter(data_rt);
        MultiSnapRecyclerView rtRecyclerView = (MultiSnapRecyclerView) rootView.findViewById(R.id.rviewBerandaRumahTangga);
        LinearLayoutManager rtManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        rtRecyclerView.setLayoutManager(rtManager);
        rtRecyclerView.setAdapter(rtAdapter);

        HorizontalAdapter makananAdapter = new HorizontalAdapter(data_makanan);
        MultiSnapRecyclerView makananRecyclerView = (MultiSnapRecyclerView) rootView.findViewById(R.id.rviewBerandaMakanan);
        LinearLayoutManager makananManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        makananRecyclerView.setLayoutManager(makananManager);
        makananRecyclerView.setAdapter(makananAdapter);

        HorizontalAdapter kecantikanAdapter = new HorizontalAdapter(data_kecantikan);
        MultiSnapRecyclerView kecantikanRecyclerView = (MultiSnapRecyclerView) rootView.findViewById(R.id.rviewBerandaKecantikan);
        LinearLayoutManager kecantikanManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        kecantikanRecyclerView.setLayoutManager(kecantikanManager);
        kecantikanRecyclerView.setAdapter(kecantikanAdapter);

        HorizontalAdapter bpriaAdapter = new HorizontalAdapter(data_busanapria);
        MultiSnapRecyclerView bpriaRecyclerView = (MultiSnapRecyclerView) rootView.findViewById(R.id.rviewBerandaBusanaPria);
        LinearLayoutManager bpriaManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        bpriaRecyclerView.setLayoutManager(bpriaManager);
        bpriaRecyclerView.setAdapter(bpriaAdapter);

        HorizontalAdapter bwanitaAdapter = new HorizontalAdapter(data_busanawanita);
        MultiSnapRecyclerView bwanitaRecyclerView = (MultiSnapRecyclerView) rootView.findViewById(R.id.rviewBerandaBusanWanita);
        LinearLayoutManager bwanitaManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        bwanitaRecyclerView.setLayoutManager(bwanitaManager);
        bwanitaRecyclerView.setAdapter(bwanitaAdapter);

        btnBayarPln     = (Button) rootView.findViewById(R.id.btnPembayaranListrik);
        btnBayarPulsa   = (Button) rootView.findViewById(R.id.btnPembayaranPulsa);
        btnBayarVoucher = (Button) rootView.findViewById(R.id.btnPembayaranVoucher);
        btnBayarPdam    = (Button) rootView.findViewById(R.id.btnPembayaranPdam);
        btnBayarTagihan = (Button) rootView.findViewById(R.id.btnPembayaranTagihan);
        btnBayarBpjs    = (Button) rootView.findViewById(R.id.btnPembayaranBpjs);
        btnBayarTv      = (Button) rootView.findViewById(R.id.btnPembayaranTv);
        btnBayarTelkom  = (Button) rootView.findViewById(R.id.btnPembayaranTelkom);
        btnBayarPaket   = (Button) rootView.findViewById(R.id.btnPembayaranPaket);

        btnBayarPln.setOnClickListener(this);
        btnBayarPulsa.setOnClickListener(this);
        btnBayarVoucher.setOnClickListener(this);
        btnBayarPdam.setOnClickListener(this);
        btnBayarTagihan.setOnClickListener(this);
        btnBayarBpjs.setOnClickListener(this);
        btnBayarTv.setOnClickListener(this);
        btnBayarTelkom.setOnClickListener(this);
        btnBayarPaket.setOnClickListener(this);

        relativeLayout = (RelativeLayout) rootView.findViewById(R.id.rltvLayoutBeranda);
        relativeLayout.requestFocus();

        return rootView;
    }

    private void init_flipper() {
        viewFlipper.setAutoStart(true);
        viewFlipper.setFlipInterval(4000);
        viewFlipper.setInAnimation(AnimationUtils.loadAnimation(context, android.R.anim.fade_in));
        viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(context, android.R.anim.fade_out));
        viewFlipper.startFlipping();
    }

    @Override
    public void onClick(View v) {
        if (prefs.getString("auth_id", "").equals("") ) {
            AlertDialog dialog = new AlertDialog.Builder(context)
                    .setTitle("Khusus Member")
                    .setMessage("Silahkan login menggunakan akun anda untuk melanjutkan")
                    .setPositiveButton("Login", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Fragment fragment = new Login();
                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                            fragmentManager.beginTransaction()
                                    .replace(R.id.content_frame, fragment)
                                    .commit();

                        }
                    })
                    .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create();
            dialog.show();
            return;
        }
        Intent intent = null;

        switch ( v.getId() ) {
            case R.id.btnPembayaranListrik:
                intent = new Intent(context, TransPln.class);
                break;

            case R.id.btnPembayaranPulsa:
                intent = new Intent(context, TransPulsa.class);
                break;

            case R.id.btnPembayaranVoucher:
                intent = new Intent(context, TransVoucher.class);
                break;

            case R.id.btnPembayaranPdam:
                intent = new Intent(context, TransPdam.class);
                break;

            case R.id.btnPembayaranTagihan:
                intent = new Intent(context, TransTagihan.class);
                break;

            case R.id.btnPembayaranBpjs:
                intent = new Intent(context, TransBpjs.class);
                break;

            case R.id.btnPembayaranTv:
                intent = new Intent(context, TransTv.class);
                break;

            case R.id.btnPembayaranTelkom:
                intent = new Intent(context, TransTelkom.class);
                break;

            case R.id.btnPembayaranPaket:
                intent = new Intent(context, TransPaket.class);
                break;
        }
        startActivity(intent);
    }
}
