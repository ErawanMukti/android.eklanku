package erris.eklanku.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import erris.eklanku.R;
import erris.eklanku.model.DataMember;
import erris.eklanku.model.LoginResponse;
import erris.eklanku.rest.ApiClient;
import erris.eklanku.rest.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoAccount extends Fragment {

    Context context;
    SharedPreferences prefs;
    TextView lblId, lblNama, lblShop, lblSaldo, lblEmail, lblPhone, lblAddress;
    String id;
    ApiInterface mApiInterface;
    Dialog loadingDialog;

    public InfoAccount() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_info_account, container, false);

        context    = getActivity();
        prefs      = getActivity().getSharedPreferences("app", Context.MODE_PRIVATE);
        lblId      = (TextView) rootView.findViewById(R.id.lblAccountId);
        lblNama    = (TextView) rootView.findViewById(R.id.lblAccountName);
        lblShop    = (TextView) rootView.findViewById(R.id.lblAccountShop);
        lblSaldo   = (TextView) rootView.findViewById(R.id.lblAccountSaldo);
        lblEmail   = (TextView) rootView.findViewById(R.id.lblAccountEmail);
        lblPhone   = (TextView) rootView.findViewById(R.id.lblAccountPhone);
        lblAddress = (TextView) rootView.findViewById(R.id.lblAccountAddress);
        id         = prefs.getString("auth_id", "-");

        mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        get_data();

        return rootView;
    }

    private void get_data() {
        loadingDialog = ProgressDialog.show(context, "Harap Tunggu", "Registrasi User...");
        loadingDialog.setCanceledOnTouchOutside(true);

        Call<LoginResponse> memberCall = mApiInterface.getAccount(id);
        memberCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                loadingDialog.dismiss();
                if (response.isSuccessful()) {
                    String status = response.body().getStatus();
                    String error  = response.body().getError();

                    if ( status.equals("OK") ) {
                        List<DataMember> member = response.body().getMember();
                        lblId.setText(member.get(0).getEklankuId());
                        lblNama.setText(member.get(0).getName());
                        lblShop.setText(member.get(0).getShopProvince() + " - " + member.get(0).getShopRegency());
                        lblSaldo.setText(String.valueOf(member.get(0).getWallet()));
                        lblEmail.setText(member.get(0).getEmail());
                        lblPhone.setText(member.get(0).getPhone());
                        lblAddress.setText("-");
                    } else {
                        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, getResources().getString(R.string.error_api), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                loadingDialog.dismiss();
                Toast.makeText(context, getResources().getString(R.string.error_api), Toast.LENGTH_SHORT).show();
                Log.d("API_GETACCOUNT", t.getMessage().toString());
            }
        });
    }
}
