package erris.eklanku;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashScreen extends AppCompatActivity {

    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        prefs = getSharedPreferences("app", Context.MODE_PRIVATE);

        Handler h = new Handler();
        h.postDelayed(splash, 1000);
    }

    Runnable splash = new Runnable() {
        @Override
        public void run() {
            Intent i = new Intent(getBaseContext(), HalamanUtama.class);
            startActivity(i);
            finish();
        }
    };
}
