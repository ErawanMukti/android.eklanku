package erris.eklanku;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import erris.eklanku.fragment.Beranda;
import erris.eklanku.fragment.Daftar;
import erris.eklanku.fragment.InfoAccount;
import erris.eklanku.fragment.LapTransaksi;
import erris.eklanku.fragment.Login;

public class HalamanUtama extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    String auth_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halaman_utama);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();*/
        prefs   = getSharedPreferences("app", Context.MODE_PRIVATE);
        auth_id = prefs.getString("auth_id", "");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if ( auth_id.isEmpty() ) {
            navigationView.inflateMenu(R.menu.drawer_guest);
        } else {
            navigationView.inflateMenu(R.menu.drawer_user);
        }
        navigationView.setNavigationItemSelectedListener(this);

        Fragment fragment = new Beranda();
        set_fragment(fragment);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.halaman_utama, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cart) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;

        if (id == R.id.nav_ghome || id == R.id.nav_uhome) {
            fragment = new Beranda();
            set_fragment(fragment);
        } else if (id == R.id.nav_gdaftar) {
            fragment = new Daftar();
            set_fragment(fragment);
        } else if (id == R.id.nav_glogin) {
            fragment = new Login();
            set_fragment(fragment);
        } else if (id == R.id.nav_gkategori || id == R.id.nav_ukategori || id == R.id.nav_utarik || id == R.id.nav_uiklan) {
            Intent in_coming = new Intent(getBaseContext(), ComingSoon.class);
            startActivity(in_coming);
        } else if (id == R.id.nav_upersonal) {
            fragment = new InfoAccount();
            set_fragment(fragment);
        } else if (id == R.id.nav_utopup) {
            Intent in_coming = new Intent(getBaseContext(), ComingSoon.class);
            startActivity(in_coming);
        } else if (id == R.id.nav_ulaptrans) {
            fragment = new LapTransaksi();
            set_fragment(fragment);
        } else if (id == R.id.nav_ukeluar) {
            editor = prefs.edit();
            editor.clear();
            editor.commit();

            Intent in_beranda = new Intent(getBaseContext(), HalamanUtama.class);
            startActivity(in_beranda);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void set_fragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();

    }
}
